class Settings

# Полученные параметры от камеры
  param: []
  
  # Параметры, введённые пользователем
  tempParam: []
  
  # Список параметров, которые необходимо получить от камеры
  getNames: []
  
  # Список изменяемых параметров, которые необходимо записать обратно на камеру
  setNames: []
  
  # 
  getParamFromCamera: () ->
    # Собираем url запроса
    url = "vb.htm?"
    i=0
    for parameter in @getNames
      url += "paratest=#{parameter}"
      if ++i != @getNames.length then url += "&"
      
    onSuccess = (request) =>
      @loadPageScript(request);
      writeToLog("Запрос выполнен успешно!")
      return      
    onError = () ->
      writeToLog("Ошибка при выполнении запроса!")
      return    
    doGetRequest(url, onSuccess, onError)
    
    return
    
  loadPageScript: (response) ->
    
    # После получения ответа от камеры, запускаем этот скрипт
    # Парсим параметры и сохраняем их в глобальную переменную param[]
    @parseParams response
    
    # Парсим имена для селектов видеокодеков
    @parseAllNames()
    
    # Создаём копию параметров для хранения введеных пользователем значений
    @tempParam = clone(@param)
    
    # Создаём селекты в соответствии с текущими значениями
    @createSelects @param
    
    # Отображаем полученные параметры и селекты
    @displayParamSet @param
    
    # Дополнительные действия, необходимые после получения ответа от камеры
    @optionalUpdate()
    return
    
  # Функция, которая разбивает строку ответа от камеры на отдельные параметры и 
  # записывает их в ассоциативный массив param[key]=value
  parseParams: (response) ->
    tmp = new Array() # два вспомогательных
    tmp2 = new Array() # массива
    unless response is ""
      tmp = (response.substr(3)).split("OK ")
      i = 0

      while i < tmp.length
        index = tmp[i].indexOf("=")
        tmp2[0] = tmp[i].slice(0, index)
        tmp2[1] = tmp[i].slice(index + 1, tmp[i].length - 1)
        @param[tmp2[0]] = tmp2[1]
        i++
    else
      writeToLog "Error: empty response from camera!"
    return
    
  # Функция, которая парсит имена для селектов 
  parseAllNames: () -> # Необходимо определить внутри экземпляра

  #Парсинг одновложенных параметров (разделитель ";")
  parseNamesUno: (key, separator) ->
    
    # Значение по-умолчанию:
    separator ?= ";"
    
    tmp = new Array()
    keyString = @param[key]
    keyParsed = key + "Parsed"
    unless keyString is ""
      @param[keyParsed] = new Array()
      tmp = (keyString).split(separator)
      
      i = 0
      while i < tmp.length
        @param[keyParsed][i] = tmp[i]
        i++
        
    return
  
  #Парсинг двухвложенных параметров (разделители "@", ";")
  parseNamesDuo: (key) ->
  
    tmp = new Array() # два вспомогательных
    tmp2 = new Array() # массива
    keyString = @param[key]
    keyParsed = key + "Parsed"
    unless keyString is ""
      @param[keyParsed] = new Array()
      tmp = (keyString).split("@")
      
      i = 0
      while i < tmp.length
        tmp2 = tmp[i].split(";")
        @param[keyParsed][i] = new Array()
        
        j = 0
        while j < tmp2.length
          @param[keyParsed][i][j] = tmp2[j]
          j++
          
        i++
    return

  
  #Парсинг трёхвложенных параметров (разделители "@", ";", ",")
  parseNamesTrio: (key) ->
    tmp = new Array() # три вспомогательных
    tmp2 = new Array() # массива
    tmp3 = new Array() # массива
    keyString = @param[key]
    keyParsed = key + "Parsed"
    unless keyString is ""
      @param[keyParsed] = new Array()
      tmp = (keyString).split("@")

      i = 0
      while i < tmp.length
        tmp2 = tmp[i].split(";")
        @param[keyParsed][i] = new Array()
        
        j = 0
        while j < tmp2.length
          tmp3 = tmp2[j].split(",")
          @param[keyParsed][i][j] = new Array()
          
          k = 0
          while k < tmp3.length
            @param[keyParsed][i][j][k] = tmp3[k]
            k++
          j++
        i++
    return
    
  # Функция, созддающая селект
  createSelect: (key, Names) ->
    try
      
      #Выбираем селекты из DOM
      Select = document.getElementById(key)      
      #Очищаем
      Select.options.length = 0   
      
      #Создаём новые элементы										
      i = 0
      while i < Names.length
        Select.options[i] = new Option(Names[i], i)
        i++
    return

  #
  createSelects: () -> # Необходимо определить в экзепляре
  
  #
  displayParamSet: (paramSet) ->
    for key in @setNames
      try
        obj = document.getElementById(key)
        idTagName = obj.tagName.toLowerCase()
        if idTagName is "input"
          if obj.type is "checkbox"
            if parseInt(paramSet[key]) is 0
              obj.checked = false
              obj.value = 0
            else
              obj.checked = true
              obj.value = 1
          else
            obj.value = paramSet[key]
        else if idTagName is "select"
          obj.value = paramSet[key]
          obj.options.selectedIndex = paramSet[key]
        else
          writeToLog key + "=" + idTagName
      catch e
        writeToLog "Не могу отобразить " + key + "."
      
    return
    
  # Дополнительные действия, необходимые после получения ответа от камеры
  optionalUpdate: () -> # Необходимо определить в экзепляре
  
  readTmpParams: () ->
  
    for name in @setNames
      try
        elem = document.getElementById(name)
        #Запичиваем значение, введенное пользователем
        @tempParam[name]=elem.value
  
    # Если хоть один инпут вернул сообщение об ощибке, то возращаем false, иначе true
  checkTmpParams: () ->
  
    validity = true    
    for name in @setNames
      try 
        errMessage = document.getElementById("#{name}_errmsg").innerHTML
        validity = false  if ((errMessage isnt "") and (errMessage isnt " "))
  
    validity
    
  # Проверяем, изменились ли параметры по сравнению с исхоными
  isChanged: () ->
    @readTmpParams()
    ischaged = false
    i = 0

    while i < @setNames.length
      key = @setNames[i]
      ischaged = true unless @tempParam[key] is @param[key]
      i++
    ischaged
  
  setParamToCamera: () ->
  
    # Производим проверку корректности введенных данных
    if @checkTmpParams()
      onSuccess = (request) =>
        alert "Параметры установлены. Ответ камеры:\r\n" + request
        writeToLog "Параметры установлены. Ответ камеры:\r\n" + request
        window.unlockPage()
        @getParamFromCamera()
        return
      onError = ->
        alert "Ошибка! Запись параметров на камеру не выполнена."
        writeToLog "Ошибка! Запись параметров на камеру не выполнена."
        window.unlockPage()
        return
        
      # Читаем значения, введенные пользователем:
      @readTmpParams()
      # Формируем строку запроса
      url = "vb.htm?"
      i = 0
      while i < @setNames.length
        try
          # Получаем значение, полученное от камеры:
          paramValue = @param[@setNames[i]]
          # Получаем значение, введенное пользователем:
          tmpParamValue = @tempParam[@setNames[i]]
          url += "&"  if url isnt "vb.htm?"
          url += @setNames[i] + "=" + tmpParamValue
        i++
      
      # Блокируем страницу
      window.lockPage()
      
      # Отправляем запрос
      doGetRequest url, onSuccess, onError
    else
      alert "Ошибка! Проверьте введенные данные."
    return

class videoSettings extends Settings
  
  constructor: () ->
    @getNames = ["title",
      "videocodec",
      "videocodecname",
      "videocodeccombo",
      "videocodeccomboname",
      "videocodecres",
      "videocodecresname",
      "bitrate1",
      "bitrate2",
      "framerate1",
      "frameratenameall1",
      "framerate2",
      "frameratenameall2",
      "framerate3",
      "frameratenameall3",
      "ratecontrol1",
      "ratecontrol2",
      "ratecontrolname", #OFF;VBR;CBR
      "datestampenable1",
      "datestampenable2",
      "datestampenable3",
      "timestampenable1",
      "timestampenable2",
      "timestampenable3",
      "textenable1",
      "textenable2",
      "textenable3",
      "textposition1",
      "textposition2",
      "textposition3",
      "textpositionname", #Top-Left;Top-Right
      "overlaytext1",
      "overlaytext2",
      "overlaytext3",
      "detailinfo1",
      "detailinfo2",
      "detailinfo3",
      "mirctrl",
      "mirctrlname", #OFF;HORIZONTAL;VERTICAL;BOTH
      "localdisplay",
      "localdisplayname", #OFF;NTSC;PAL
      "livequality"]
      
    @setNames = ["title", 
      "videocodec", 
      "videocodeccombo", 
      "videocodecres", 
      "bitrate1", 
      "framerate1",
      "ratecontrol1",
      "ratecontrol2",
      "bitrate2", 
      "framerate2",
      "framerate3",
      "livequality",
      "overlaytext1",
      "overlaytext2",
      "overlaytext3",
      "textenable1",
      "textenable2",
      "textenable3",
      "textposition1",
      "textposition2",
      "textposition3",
      "detailinfo1",
      "detailinfo2",
      "detailinfo3",
      "datestampenable1",
      "datestampenable2",
      "datestampenable3",
      "timestampenable1",
      "timestampenable2",
      "timestampenable3"]
  
  # Определяем функцию, которая парсит имена кодеков 
  parseAllNames: () ->  
    # Эта переменная нам нужна для правильного отображения трёх потоков
    createStreamFormat = () =>
      streamFormat = new Array()
      
      i = 0
      while i < @param["videocodecresnameParsed"].length
        streamFormat[i] = new Array()
        
        j = 0
        while j < @param["videocodecresnameParsed"][i].length
          streamFormat[i][j] = 0
          streamFormat[i][j] = 1  if (i is 2) and ( (j is 1) or (i is 3) or (i is 5) )
          streamFormat[i][j] = 2  if (i is 3) or (i is 4)
          streamFormat[i][j] = 3  if (i is 5) or (i is 6) or (i is 7)
          streamFormat[i][j] = 4  if (i is 8) or (i is 9)
          j++
        i++
      streamFormat
    try
      @parseNamesUno "videocodecname"
      @parseNamesDuo "videocodeccomboname"
      @parseNamesDuo "videocodecresname"
      @parseNamesTrio "frameratenameall1"
      @parseNamesTrio "frameratenameall2"
      @parseNamesTrio "frameratenameall3"
      @parseNamesUno "ratecontrolname"
      @parseNamesUno "textpositionname"
      @parseNamesUno "mirctrlname"
      @parseNamesUno "localdisplayname"
      @param["streamformat"] = createStreamFormat()
    catch e
      writeToLog "Cannot parse videocodec names."
    return
  
  
  # Определяем функцию, которая создаёт селекты на странице 
  createSelects: (paramSet) ->
    stream1Mpeg = "<div class='stream' id='stream1'>" + "<h3> Поток 1 </h3>" + "<div class='form_item'>" + "<label> framerate1 </label>" + "<select id='framerate1' onchange=''>" + "<option value = '-1'> Loading... </option>" + "</select>" + "</div>" + "<div class = 'p4px'></div>" + "<div class='form_item'>" + "<label> bitrate1 </label>" + "<input id='bitrate1' class='bitrate' required value = 'Loading...' " + "oninput='validateInput(\"bitrate1\")' onchange='validateInput(\"bitrate1\")'>" + "<div id='bitrate1_errmsg' class='error-message hidden'></div>" + "</div>" + "<div class='p4px'></div>" + "<div class='form_item'>" + "<label> ratecontrol1 </label>" + "<select id='ratecontrol1' onchange=''>" + "<option value = '0'> OFF </option>" + "<option value = '1'> VBR </option>" + "<option value = '2'> CBR </option>" + "</select>" + "</div>" + "<div class='p4px'></div>" + "<div class='form_item'>" + "<label> textenable1 </label>" + "<input type='checkbox' onchange='setCheckboxValue(\"textenable1\");' " + "id='textenable1' value='0' >" + "</div>" + "<div class='p4px'></div>" + "<div class='form_item'>" + "<label> textposition1 </label>" + "<select id='textposition1' onchange=''>" + "<option value = '0'> Top-Left </option>" + "<option value = '1'> Top-Right </option>" + "</select>" + "</div>" + "<div class='p4px'></div>" + "<div class='form_item'>" + "<label> overlaytext1 </label>" + "<input id='overlaytext1' class='text' value='Loading...' " + "oninput='validateInput(\"overlaytext1\")' onchange='validateInput(\"overlaytext1\")'>" + "<div id='overlaytext1_errmsg' class='error-message hidden'></div>" + "</div>" + "<div class='p4px'></div>" + "<div class='form_item'>" + "<label> detailinfo1 </label>" + "<input type='checkbox' onchange='setCheckboxValue(\"detailinfo1\");' " + "id='detailinfo1' value='0' >" + "</div>" + "<div class='p4px'></div>" + "<div class='form_item'>" + "<label> datestampenable1 </label>" + "<input type='checkbox' onchange='setCheckboxValue(\"datestampenable1\");' " + "id='datestampenable1' value='0' >" + "</div>" + "<div class='p4px'></div>" + "<div class='form_item'>" + "<label> timestampenable1 </label>" + "<input type='checkbox' onchange='setCheckboxValue(\"timestampenable1\");' " + "id='timestampenable1' value='0' >" + "</div>" + "<div class='p4px'></div>" + "</div>"
    stream1Jpeg = "<div class='stream' id='stream1'>" + "<h3> Поток 1 </h3>" + "<div class='form_item'>" + "<label> framerate1 </label>" + "<select id='framerate1' onchange=''>" + "<option value = '-1'> Loading... </option>" + "</select>" + "</div>" + "<div class = 'p4px'></div>" + "<div class='form_item'>" + "<label> livequality </label>" + "<input id = 'livequality' class='livequality' value = 'Loading...' " + "oninput='validateInput(\"livequality\")' onchange='validateInput(\"livequality\")'>" + "<div id='livequality_errmsg' class='error-message hidden'>" + "</div>" + "<div class = 'p4px'></div>" + "<div class='form_item'>" + "<label> textenable1 </label>" + "<input type='checkbox' onchange='setCheckboxValue(\"textenable1\");' id='textenable1' value='0' >" + "</div>" + "<div class='p4px'></div>" + "<div class='form_item'>" + "<label> textposition1 </label>" + "<select id='textposition1' onchange=''>" + "<option value = '0'> Top-Left </option>" + "<option value = '1'> Top-Right </option>" + "</select>" + "</div>" + "<div class='p4px'></div>" + "<div class='form_item'>" + "<label> overlaytext1 </label>" + "<input id='overlaytext1' class='text' value='Loading...' " + "oninput='validateInput(\"overlaytext1\")' onchange='validateInput(\"overlaytext1\")'>" + "<div id='overlaytext1_errmsg' class='error-message hidden'></div>" + "</div>" + "<div class='p4px'></div>" + "<div class='form_item'>" + "<label> detailinfo1 </label>" + "<input type='checkbox' onchange='setCheckboxValue(\"detailinfo1\");' id='detailinfo1' value='0' >" + "</div>" + "<div class='p4px'></div>" + "<div class='form_item'>" + "<label> datestampenable1 </label>" + "<input type='checkbox' onchange='setCheckboxValue(\"datestampenable1\");' id='datestampenable1' value='0' >" + "</div>" + "<div class='p4px'></div>" + "<div class='form_item'>" + "<label> timestampenable1 </label>" + "<input type='checkbox' onchange='setCheckboxValue(\"timestampenable1\");' id='timestampenable1' value='0' >" + "</div>" + "<div class='p4px'></div>" + "</div>"
    stream2Jpeg = "<div class='stream' id='stream2'>" + "<h3> Поток 2 </h3>" + "<div class='form_item'>" + "<label> framerate2 </label>" + "<select id='framerate2' onchange=''>" + "<option value = '-1'> Loading... </option>" + "</select>" + "</div>" + "<div class = 'p4px'></div>" + "<div class='form_item'>" + "<label> livequality </label>" + "<input id = 'livequality' class='livequality' value = 'Loading...' " + "oninput='validateInput(\"livequality\")' onchange='validateInput(\"livequality\")'>" + "<div id='livequality_errmsg' class='error-message hidden'></div>" + "</div>" + "<div class = 'p4px'></div>" + "<div class='form_item'>" + "<label> textenable2 </label>" + "<input type='checkbox' onchange='setCheckboxValue(\"textenable2\");' id='textenable2' value='0' >" + "</div>" + "<div class='p4px'></div>" + "<div class='form_item'>" + "<label> textposition2 </label>" + "<select id='textposition2' onchange=''>" + "<option value = '0'> Top-Left </option>" + "<option value = '1'> Top-Right </option>" + "</select>" + "</div>" + "<div class='p4px'></div>" + "<div class='form_item'>" + "<label> overlaytext1 </label>" + "<input id='overlaytext2' class='text' value='Loading...' " + "oninput='validateInput(\"overlaytext2\")' onchange='validateInput(\"overlaytext2\")'>" + "<div id='overlaytext2_errmsg' class='error-message hidden'></div>" + "</div>" + "<div class='p4px'></div>" + "<div class='form_item'>" + "<label> detailinfo2 </label>" + "<input type='checkbox' onchange='setCheckboxValue(\"detailinfo2\");' id='detailinfo2' value='0' >" + "</div>" + "<div class='p4px'></div>" + "<div class='form_item'>" + "<label> datestampenable2 </label>" + "<input type='checkbox' onchange='setCheckboxValue(\"datestampenable2\");' id='datestampenable2' value='0' >" + "</div>" + "<div class='p4px'></div>" + "<div class='form_item'>" + "<label> timestampenable2 </label>" + "<input type='checkbox' onchange='setCheckboxValue(\"timestampenable2\");' id='timestampenable2' value='0' >" + "</div>" + "<div class='p4px'></div>" + "</div>"
    stream2Mpeg = "<div class='stream' id='stream2'>" + "<h3> Поток 2 </h3>" + "<div class='form_item'>" + "<label> framerate2 </label>" + "<select id='framerate2' onchange=''>" + "<option value = '-1'> Loading... </option>" + "</select>" + "</div>" + "<div class = 'p4px'></div>" + "<div class='form_item'>" + "<label> bitrate2 </label>" + "<input id='bitrate2' value = 'Loading...' >" + "</div>" + "<div class='p4px'> </div>" + "<div class='form_item'><label> ratecontrol2 </label>" + "<select id='ratecontrol2' onchange=''>" + "<option value = '0'> OFF </option>" + "<option value = '1'> VBR </option>" + "<option value = '2'> CBR </option>" + "</select>" + "</div>" + "<div class='p4px'> </div>" + "<div class='form_item'>" + "<label> textenable2 </label>" + "<input type='checkbox' onchange='setCheckboxValue(\"textenable2\");' " + "id='textenable2' value='0' >" + "</div>" + "<div class='p4px'></div>" + "<div class='form_item'>" + "<label> textposition2 </label>" + "<select id='textposition2' onchange=''>" + "<option value = '0'> Top-Left </option>" + "<option value = '1'> Top-Right </option>" + "</select>" + "</div>" + "<div class='p4px'></div>" + "<div class='form_item'>" + "<label> overlaytext2 </label>" + "<input id='overlaytext2' class='text' value='Loading...' " + "oninput='validateInput(\"overlaytext2\")' onchange='validateInput(\"overlaytext2\")'>" + "</div>" + "<div id='overlaytext2_errmsg' class='error-message hidden'></div>" + "<div class='p4px'></div>" + "<div class='form_item'>" + "<label> detailinfo2 </label>" + "<input type='checkbox' onchange='setCheckboxValue(\"detailinfo2\");' " + "id='detailinfo2' value='0' >" + "</div>" + "<div class='p4px'></div>" + "<div class='form_item'>" + "<label> datestampenable2 </label>" + "<input type='checkbox' onchange='setCheckboxValue(\"datestampenable2\");' " + "id='datestampenable2' value='0' >" + "</div>" + "<div class='p4px'></div>" + "<div class='form_item'>" + "<label> timestampenable2 </label>" + "<input type='checkbox' onchange='setCheckboxValue(\"timestampenable2\");' " + "id='timestampenable2' value='0' >" + "</div>" + "<div class='p4px'></div>" + "</div>"
    stream3Mpeg = "<div class='stream' id='stream3'>" + "<h3> Поток 3 </h3>" + "<div class='form_item'>" + "<label> framerate3 </label>" + "<select id='framerate3' onchange=''>" + "<option value = '-1'> Loading... </option>" + "</select>" + "</div>" + "<div class = 'p4px'></div>" + "<div class='form_item'>" + "<label> bitrate1 </label>" + "<input id='bitrate2' class='bitrate' required value = 'Loading...' " + "oninput='validateInput(\"bitrate2\")' onchange='validateInput(\"bitrate2\")'>" + "<div id='bitrate2_errmsg' class='error-message hidden'></div>" + "</div>" + "<div class='p4px'> </div>" + "<div class='form_item'>" + "<label> ratecontrol2 </label>" + "<select id='ratecontrol2' onchange=''>" + "<option value = '0'> OFF </option>" + "<option value = '1'> VBR </option>" + "<option value = '2'> CBR </option>" + "</select>" + "</div>" + "<div class='p4px'> </div>" + "<div class='form_item'>" + "<label> textenable3 </label>" + "<input type='checkbox' onchange='setCheckboxValue(\"textenable3\");' " + "id='textenable3' value='0' >" + "</div>" + "<div class='p4px'></div>" + "<div class='form_item'>" + "<label> textposition3 </label>" + "<select id='textposition3' onchange=''>" + "<option value = '0'> Top-Left </option>" + "<option value = '1'> Top-Right </option>" + "</select>" + "</div>" + "<div class='p4px'></div>" + "<div class='form_item'>" + "<label> overlaytext3 </label>" + "<input id='overlaytext3' class='text' value='Loading...' " + "oninput='validateInput(\"overlaytext3\")' onchange='validateInput(\"overlaytext3\")'>" + "<div id='overlaytext3_errmsg' class='error-message hidden'></div>" + "</div>" + "<div class='p4px'></div>" + "<div class='form_item'>" + "<label> detailinfo3 </label>" + "<input type='checkbox' onchange='setCheckboxValue(\"detailinfo3\");' " + "id='detailinfo3' value='0' >" + "</div>" + "<div class='p4px'></div>" + "<div class='form_item'>" + "<label> datestampenable3 </label>" + "<input type='checkbox' onchange='setCheckboxValue(\"datestampenable3\");' " + "id='datestampenable3' value='0' >" + "</div>" + "<div class='p4px'></div>" + "<div class='form_item'>" + "<label> timestampenable3 </label>" + "<input type='checkbox' onchange='setCheckboxValue(\"timestampenable3\");' " + "id='timestampenable3' value='0' >" + "</div>" + "<div class='p4px'></div>" + "</div>"

    showStreams = (streamformat) ->
      streamsDiv = document.getElementById("streams")
      switch streamformat
        when 0
          streamsDiv.innerHTML = stream1Mpeg

        #writeToLog("Значение \"streamformat: \""+streamformat);
        when 1
          streamsDiv.innerHTML = stream1Jpeg

        #writeToLog("Значение \"streamformat: \""+streamformat);
        when 2
          streamsDiv.innerHTML = stream1Mpeg + stream2Jpeg

        #writeToLog("Значение \"streamformat: \""+streamformat);
        when 3
          streamsDiv.innerHTML = stream1Mpeg + stream2Mpeg

        #writeToLog("Значение \"streamformat: \""+streamformat);
        when 4
          streamsDiv.innerHTML = stream1Mpeg + stream2Jpeg + stream3Mpeg

        #writeToLog("Значение \"streamformat: \""+streamformat);
        else
          writeToLog "Ошибка отображения потоков"
      return
    
      
    #Получаем индексы для извлечения имён селектов	
    videocodecNum = parseInt(paramSet["videocodec"])
    videocodeccomboNum = parseInt(paramSet["videocodeccombo"])
    videocodecresNum = parseInt(paramSet["videocodecres"])
    videocodecresIndex = ((if videocodecNum is 0 then videocodeccomboNum else ((if videocodecNum is 1 then 3 + videocodeccomboNum else 8 + videocodeccomboNum))))

    #Получаем имена для селектов	
    videocodecNames = paramSet["videocodecnameParsed"]
    videocodeccomboNames = paramSet["videocodeccombonameParsed"][videocodecNum]
    videocodecresNames = paramSet["videocodecresnameParsed"][videocodecresIndex]
    framerate1Names = paramSet["frameratenameall1Parsed"][videocodecresIndex][videocodecresNum]
    framerate2Names = paramSet["frameratenameall2Parsed"][videocodecresIndex][videocodecresNum]
    framerate3Names = paramSet["frameratenameall3Parsed"][videocodecresIndex][videocodecresNum]
    try
      showStreams paramSet["streamformat"][videocodecresIndex][videocodecresNum]
      @createSelect "videocodec", videocodecNames
      @createSelect "videocodeccombo", videocodeccomboNames
      @createSelect "videocodecres", videocodecresNames
      @createSelect "framerate1", framerate1Names
      @createSelect "framerate2", framerate2Names
      @createSelect "framerate3", framerate3Names
    catch e
      writeToLog "Cannot find one of videocodec selects."
    
  onCodecSelectChange: (triger) ->
    try
      if triger is 2
        videocodecSelect = document.getElementById("videocodec")
        videocodeccomboSelect = document.getElementById("videocodeccombo")
        videocodecresSelect = document.getElementById("videocodecres")
        @tempParam["videocodec"] = videocodecSelect.value
        @tempParam["videocodeccombo"] = videocodeccomboSelect.value
        @tempParam["videocodecres"] = videocodecresSelect.value
      else if triger is 1
        videocodecSelect = document.getElementById("videocodec")
        videocodeccomboSelect = document.getElementById("videocodeccombo")
        @tempParam["videocodec"] = videocodecSelect.value
        @tempParam["videocodeccombo"] = videocodeccomboSelect.value
        @tempParam["videocodecres"] = 0
      else if triger is 0
        videocodecSelect = document.getElementById("videocodec")
        @tempParam["videocodec"] = videocodecSelect.value
        @tempParam["videocodeccombo"] = 0
        @tempParam["videocodecres"] = 0
      @tempParam["framerate1"] = 0
      @tempParam["framerate2"] = 0
      @tempParam["framerate3"] = 0
      @createSelects @tempParam
      @displayParamSet @tempParam
    return
    
 # Сетевые настройки 
class networkSettings extends Settings
  constructor: () ->
    @getNames = ["title"
      "netip"
      "netmask"
      "gateway"
      "dnsip"
      "ftpip"
      "ftpipport"
      "ftpuser"
      "ftppassword"
      "ftppath"
      "maxftpuserlen"
      "maxftppwdlen"
      "maxftppathlen"
      "smtpauth"
      "smtpuser"
      "maxsmtpuser"
      "smtppwd"
      "maxsmtppwd" #OFF;VBR;CBR
      "smtpsender"
      "maxsmtpsender"
      "smtpip"
      "smtpport"
      "emailuser"
      "maxemailuserlen"
      "multicast"
      "sntpip"
      "httpport"
      "httpsport"]
    @setNames = ["netip"
      "netmask"
      "gateway"
      "dnsip"
      "ftpip"
      "ftpipport"
      "ftppath"
      "ftppassword"
      "ftpuser"
      "smtpauth"
      "smtpuser"
      "smtppwd"
      "smtpsender"
      "smtpip"
      "smtpport"
      "emailuser"
      "sntpip"    ]
  parseAllNames: ->
    
    # Функция ощищает лишние нули в IP-адресе, например:
    # 192.168.000.001 -> 192.168.0.1
    parseIPadress = (ipadress) ->
      tmp = new Array()
      unless ipadress is ""
        tmp = (ipadress).split(".")
        if tmp.length isnt 4
          ipadress
        else
          clearip = "" + parseInt(tmp[0]) + "." + parseInt(tmp[1]) + "." + parseInt(tmp[2]) + "." + parseInt(tmp[3])
          clearip
      else
        ipadress
    @param["netip"] = parseIPadress(@param["netip"])
    @param["netmask"] = parseIPadress(@param["netmask"])
    @param["gateway"] = parseIPadress(@param["gateway"])
    @param["dnsip"] = parseIPadress(@param["dnsip"])
    @param["ftpip"] = parseIPadress(@param["ftpip"])
    @param["smtpip"] = parseIPadress(@param["smtpip"])
    @param["sntpip"] = parseIPadress(@param["sntpip"])
    return

# Параметры камеры
class cameraSettings extends Settings
  constructor: ->
    @getNames = ["title"
      "brightness"
      "contrast"
      "saturation"
      "sharpness"
      "colorkiller"
      "exposurectrl"
      "binning"
      "img2atype"
      "blc"
      "histogram"
      "nfltctrl"
      "tnfltctrl" ]
    @setNames = [ "brightness"
      "contrast"
      "saturation"
      "sharpness"
      "colorkiller"
      "exposurectrl"
      "binning"
      "img2atype"
      "blc"
      "histogram"
      "nfltctrl"
      "tnfltctrl"    ]
  optionalUpdate: ->
    window.transmitValue "brightness", "sliderBrightness"
    window.transmitValue "contrast", "sliderContrast"
    window.transmitValue "saturation", "sliderSaturation"
    window.transmitValue "sharpness", "sliderSharpness"
    return
    
# Живое видео
class livevideoSettings extends Settings

  constructor: ->
    @getNames = ["title"
      "videocodec"
      "streamname1"
      "streamname2"
      "streamname3" ]
    @setNames = []
  
  # Определяем функцию, которая парсит имена кодеков 
  parseAllNames: ->
    try
      @parseNamesUno "streamname1", "@"
      @parseNamesUno "streamname2", "@"
      @parseNamesUno "streamname3", "@"
    catch e
      writeToLog "Cannot parse streamnames."
    return

  createSelects: (paramSet) ->
    streamNames = []
    video = document.getElementById("video")
    i = 0

    while i <= paramSet["videocodec"]
      streamname = "streamname" + (i + 1) + "Parsed"
      streamNames[i] = paramSet[streamname][0]
      i++
    @createSelect "stream", streamNames
    src = paramSet["streamname1Parsed"][1]
    videovlc = "<embed type=\"application/x-vlc-plugin\" pluginspage=\"http://www.videolan.org\" " + "width=" + (video.offsetWidth - 2) + " height=" + parseInt(video.offsetWidth * 9 / 16) + "  src=\"" + src + "\" name=\"vlc\" />"
    videovlc += "<br>Если вы не видите видео, установите <a href=\"http://www.videolan.org/vlc\" target=\"_new\">VLC Player</a> с компонентом WebPlugin"
    video.innerHTML = videovlc
    return

  onCodecSelectChange: ->
    select = document.getElementById("stream")
    video = document.getElementById("video")
    temp = 1 + parseInt(select.value)
    streamname = "streamname" + (temp) + "Parsed"
    src = @param[streamname][1]
    videovlc = "<embed type=\"application/x-vlc-plugin\" pluginspage=\"http://www.videolan.org\" " + "width=" + (video.offsetWidth - 2) + " height=" + parseInt(video.offsetWidth * 9 / 16) + "  src=\"" + src + "\" name=\"vlc\" />"
    videovlc += "<br>Если вы не видите видео, установите <a href=\"http://www.videolan.org/vlc\" target=\"_new\">VLC Player</a> с компонентом WebPlugin"
    video.innerHTML = videovlc
    return

###
Авторизация
###

#
class Authorization 
  
  # возвращает cookie с именем name, если есть, если нет, то undefined
  getCookie: (name) ->
    matches = document.cookie.match(new RegExp("(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, "\\$1") + "=([^;]*)"))
    (if matches then decodeURIComponent(matches[1]) else `undefined`)

  
  #
  setCookie: ->
    
    # Устанавливаем таймаут в 1 час
    dateExpires = new Date(new Date().getTime() + 60 * 1000)
    document.cookie = "user=" + @username + "; expires=" + dateExpires.toUTCString()
    document.cookie = "pswrd=" + @pswrd + "; expires=" + dateExpires.toUTCString()
    return

  
  #
  show: (authMessage, user, pswrd) ->
    authMessage = authMessage or ""
    user = user or ""
    pswrd = pswrd or ""
    
    # Вывод сообщения об ошибке авторизации
    if authMessage isnt ""
      $("#authmsg").html authMessage
      $("#authmsg").removeClass "hidden"
    else
      $("#authmsg").html ""
      $("#authmsg").addClass "hidden"
    $("#authuser").val user  if user isnt ""
    $("#authpswrd").val user  if pswrd isnt ""
    $("#authorization").removeClass "hidden"
    return

  
  #
  hide: ->
    createVarsForPage "videosettings"
    showPage "videosettings"
    $("#authorization").addClass "hidden"
    setInterval (=>
      @setCookie()
      writeToLog "Куки обновлены"
      return
    ), 30 * 1000
    return

  
  #
  check: ->
    url = "vb.htm?paratest=user"
    xmlhttp = getXmlHttp()
    xmlhttp.onreadystatechange = =>
      if xmlhttp.readyState is 4
        if (xmlhttp.status is 401) or (xmlhttp.status is 403)
          @show "Проверьте правильность логина / пароля.", @username, @pswrd
        else
          @hide()
      return

    xmlhttp.open "GET", url, true
    xmlhttp.setRequestHeader "Authorization", "Basic " + base64encode(@username + ":" + @pswrd)
    xmlhttp.send()
    return

  getInput: ->
    @username = $("#authuser").val()
    @pswrd = $("#authpswrd").val()
    @setCookie()
    return

  onKeyUp: ->
    if eval(window.event.keyCode) is 13
      @getInput()
      @check()
    return

  constructor: ->
    # Получаем куки
    @username = @getCookie("user")
    @pswrd = @getCookie("pswrd")

    # Убрать 
    @username = "user"
    @pswrd = "9999"

    # Если куки получены
    if @username
      @check()
    else
      @show()      
    return


###
Ниже здесь содержатся глобальные функции, общие для всех страниц
###

#Функция для универсального копирования объектов
clone = (obj) ->
  if not obj? or typeof obj isnt 'object'
    return obj

  if obj instanceof Date
    return new Date(obj.getTime()) 

  if obj instanceof RegExp
    flags = ''
    flags += 'g' if obj.global?
    flags += 'i' if obj.ignoreCase?
    flags += 'm' if obj.multiline?
    flags += 'y' if obj.sticky?
    return new RegExp(obj.source, flags) 

  newInstance = new obj.constructor()

  for key of obj
    newInstance[key] = clone obj[key]

  return newInstance
  
# Функция кодирования строки в base64
base64encode = (str) ->  
  # Символы для base64-преобразования
  b64chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefg" + "hijklmnopqrstuvwxyz0123456789+/="
  b64encoded = ""  
  i = 0
  while i < str.length
    chr1 = str.charCodeAt(i++)
    chr2 = str.charCodeAt(i++)
    chr3 = str.charCodeAt(i++)
    enc1 = chr1 >> 2
    enc2 = ((chr1 & 3) << 4) | (chr2 >> 4)
    enc3 = (if isNaN(chr2) then 64 else (((chr2 & 15) << 2) | (chr3 >> 6)))
    enc4 = (if isNaN(chr3) then 64 else (chr3 & 63))
    b64encoded += b64chars.charAt(enc1) + b64chars.charAt(enc2) + b64chars.charAt(enc3) + b64chars.charAt(enc4)
  b64encoded
  
  
# Функция для универсального создания xmlhttp в разл. браузерах
getXmlHttp = ->
  xmlhttp = undefined
  try
    xmlhttp = new ActiveXObject("Msxml2.XMLHTTP")
  catch e
    try
      xmlhttp = new ActiveXObject("Microsoft.XMLHTTP")
    catch E
      xmlhttp = false
  xmlhttp = new XMLHttpRequest()  if not xmlhttp and typeof XMLHttpRequest isnt "undefined"
  xmlhttp
 
# Функция выполняет Get  запрос, если запрос сделан успешно, то
# выполнит doOnSuccess(xmlhttp.responseText), если нет, то 
# doOnFail. 
doGetRequest = (url, doOnSuccess, doOnFail, user, pswrd) ->
  user = user or auth.username
  pswrd = pswrd or auth.pswrd
  xmlhttp = getXmlHttp()
  xmlhttp.onreadystatechange = ->
    if xmlhttp.readyState is 4
      if xmlhttp.status is 200
        doOnSuccess xmlhttp.responseText
      else if xmlhttp.status is 401
        alert "Вы не авторизированы!"
      else
        doOnFail
    return

  xmlhttp.open "GET", url, true
  xmlhttp.setRequestHeader "Authorization", "Basic " + base64encode(user + ":" + pswrd)
  xmlhttp.send()
  return

# Возвращает текущее вермя в виде строки "ЧЧ:ММ:СС"
getCurrTime = ->
  checkTime = (i) ->
    i = "0" + i  if i < 10
    i
  t = new Date()
  timeStr = ""
  timeStr += checkTime(t.getHours()) + ":"
  timeStr += checkTime(t.getMinutes()) + ":"
  timeStr += checkTime(t.getSeconds())
  timeStr

# Функция для записи в лог
writeToLog = (errorText) ->
  try
    document.getElementById("scriptLog").innerHTML = getCurrTime() + " " + errorText + "\r\n" + document.getElementById("scriptLog").innerHTML
    #console.log(getCurrTime() + " " + errorText + "\r\n" + document.getElementById("scriptLog").innerHTML)
  return

#
window.changePageTo = (pageId) ->
  try
    if settings.isChanged()
      if confirm("Изменения не сохранены. Все равно перейти?")
        showPage pageId
        createVarsForPage pageId
    else
      showPage pageId
      createVarsForPage pageId
  catch e
    showPage pageId
    createVarsForPage pageId
  return

#
showPage = (pageId) ->
  pagesId = [
    "videosettings"
    "camerasettings"
    "networksettings"
    "livevideo"
  ]
  selectorsId = [
    "sel_videosettings"
    "sel_camerasettings"
    "sel_networksettings"
    "sel_livevideo"
  ]
  i = 0
  while i < pagesId.length
    page = document.getElementById(pagesId[i])
    selector = document.getElementById(selectorsId[i])
    if pageId is pagesId[i]
      page.className = ""
      selector.className = "l_menu_item selected"
    else
      page.className = "hidden"
      selector.className = "l_menu_item"
    i++
  return
  
#  
createVarsForPage = (pageId) ->
  switch pageId
    when "videosettings"
      window.settings = null
      window.settings = new videoSettings()
      window.settings.getParamFromCamera()
    when "camerasettings"
      window.settings = null
      window.settings = new cameraSettings()
      window.settings.getParamFromCamera()
    when "networksettings"
      window.settings = null
      window.settings = new networkSettings()
      window.settings.getParamFromCamera()
    when "livevideo"
      window.settings = null
      window.settings = new livevideoSettings()
      window.settings.getParamFromCamera()
    else
      writeToLog "Ошибка createVarsForPage:" + pageId
  return
  
# Фунция проверки корректности введенных данных
window.validateInput = (key) ->
  try       
    
       
    checkRange = (value, min, max) ->
      if (value >= min) and (value <= max)
        true
      else
        false
    # Ипут, который будем проверять    
    input = $("#" + key)
    # Див, в который будем выводить сообщение об ощибке    
    errmsgDiv = $("#" + key + "_errmsg")
    # Введённое значение: 
    value = input.val()
    
    reIPadress = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/
    reNumber = /^[0-9]+$/
    reUser = /^[a-z0-9_]+$/
    reText = /^[aA-zZ0-9_-]+$/
    rePasword = /^[aA-zZ0-9_]{4,15}$/
    reFindSpaces = /\s/
    reUrlPath = /^[a-z0-9@./_-]+$/
    errmsg = ""
    
    if (input.hasClass("ipadress"))  
      if (reIPadress.test(value)) 
        errmsg += "" 
      else 
        errmsg += "Введен не верный IP. "
      
    (if (reNumber.test(value)) then ((if checkRange(value, 64, 10000) then errmsg += "" else errmsg += "Число должно быть в диапазоне от 64 до 10000. ")) else errmsg += "Введите корректное число. ")  if input.hasClass("bitrate")
    (if (reNumber.test(value)) then ((if checkRange(value, 0, 65535) then errmsg += "" else errmsg += "Порт должен быть в диапазоне от 0 до 65535. ")) else errmsg += "Введите корректное число. ")  if input.hasClass("netport")
    (if (reNumber.test(value)) then ((if checkRange(value, 2, 97) then errmsg += "" else errmsg += "Число должно быть в диапазоне от 2 до 97. ")) else errmsg += "Введите корректное число. ")  if input.hasClass("livequality")
    (if reUser.test(value) then ((if checkRange(value.length, 3, 31) then errmsg += "" else errmsg += "Число знаков должно быть в диапазоне от 3 до 31. ")) else errmsg += "Введите корректное имя пользователя. (Только латинские буквы, цифры и знак \"_\"). ")  if input.hasClass("user")
    (if checkRange(value.length, 1, 31) then ((if reText.test(value) then errmsg += "" else errmsg += "Разрешены Только латинские буквы, цифры и знаки \"_\" и \"-\". ")) else errmsg += "Число знаков должно быть в диапазоне от 1 до 31. ")  if input.hasClass("text")
    (if checkRange(value.length, 1, 11) then ((if reText.test(value) then errmsg += "" else errmsg += "Разрешены Только латинские буквы, цифры и знаки \"_\" и \"-\". ")) else errmsg += "Число знаков должно быть в диапазоне от 1 до 11. ")  if input.hasClass("shorttext")
    (if checkRange(value.length, 1, 63) then ((if reUrlPath.test(value) then errmsg += "" else errmsg += "Введен не корректный путь. ")) else errmsg += "Количество знаков должно быть в диапазоне от 1 до 63. ")  if input.hasClass("urlpath")
    (if (rePasword.test(value)) then errmsg += "" else errmsg += "Пароль должен содержать от 4 до 15 знаков латиницы, цифр или \"_\". ")  if input.hasClass("password")
    
    #Вывод сообщения об ошибке
    errmsgDiv.prop "innerHTML", errmsg
    
    #Управление отображением блока с ошибкой
    if errmsg is ""
      errmsgDiv.addClass "hidden"
    else
      errmsgDiv.removeClass "hidden"
  return

window.setCheckboxValue = (id) ->
  try
    obj = document.getElementById(id)
    if obj.checked
      obj.value = 1
    else
      obj.value = 0
  catch e
    writeToLog "Не могу установить Checkbox c id=" + id
  return

#
window.lockPage = ->
  $("#locker").removeClass "hidden"
  return

#
window.unlockPage = ->
  $("#locker").addClass "hidden"
  return

window.transmitValue = (idFrom, idTo) ->
  $("#" + idTo).val $("#" + idFrom).val()
  return
  

#Действия после загрузки страницы:
auth = new Authorization()