#include <stdio.h>
#include <stdlib.h>

char out_buf[100];

#define OUTPUT(fmt, args...)	write(STDOUT_FILENO, out_buf, sprintf(out_buf, fmt, ##args) + 1)

int main(void)
{
	//printf("Content-type: text/html\n");
	OUTPUT("Content-type: text/html\n\n");
	OUTPUT("<html><h1>HELLO WORLD</h1>\r\n</html>");;
	return EXIT_SUCCESS;
}
